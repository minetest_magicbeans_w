-- get intllib optionally
local S
if (minetest.get_modpath("intllib")) then
	S = intllib.Getter()
else
	S = function ( s ) return s end
end
math.randomseed(os.time())

--Register beanstalk nodes
minetest.register_node("magicbeans_w:leaves", {
	description = S("Magic beanstalk leaves"),
	_doc_items_longdesc = S("Leaves from a giant magic beanstalk, a plant which grows high into the sky. It is grown from a magic beanstalk bean. Can you climb to the top?"),
	tiles = {"magicbeans_w_leaves.png"},
	inventory_image = "magicbeans_w_leaves.png",
	drawtype = "plantlike",
	is_ground_content = false,
	walkable = false,
	climbable = true,
	paramtype = "light",
	groups = {leaves = 1, snappy = 3, flammable = 2},
	sounds = default.node_sound_leaves_defaults()
})

minetest.register_node("magicbeans_w:blank", {
	description = S("Magic beanstalk climbable blank"),
	_doc_items_create_entry = false,
	drawtype = "airlike",
	inventory_image = "unknown_node.png",
	wield_image = "unknown_node.png",
	walkable = false,
	climbable = true,
	paramtype = "light",
	buildable_to = true,
	pointable = false,
	diggable = false,
	drop = "",
	groups = {not_in_creative_inventory = 1},
})

minetest.register_node("magicbeans_w:stem", {
	description = S("Magic beanstalk stem"),
	is_ground_content = false,
	_doc_items_longdesc = S("A stem from a giant magic beanstalk, a plant which grows high into the sky. It is grown from a magic beanstalk bean."),
	tiles = {"magicbeans_w_stem_topbottom.png", "magicbeans_w_stem_topbottom.png", "magicbeans_w_stem.png"},
	paramtype2 = "facedir",
	groups = {oddly_breakable_by_hand = 1, choppy = 3, flammable = 2},
	sounds = default.node_sound_wood_defaults(),
	on_place = minetest.rotate_node,
})

minetest.register_craft({
	type="fuel",
	recipe = "magicbeans_w:stem",
	burntime = 21,
})

minetest.register_node("magicbeans_w:cloud", {
	description = S("Thin Cloud"),
	_doc_items_longdesc = S("Thin clouds are rather unstable, are destroyed by a single punch and can't be collected. You can walk, climb and build on them, but be careful not to accidentaly fall through, especially when you drop on these."),
	paramtype = "light",
	tiles = {"default_cloud.png"},
	walkable = false,
	climbable = true,
	sunlight_propagates = true,
	post_effect_color = "#FFFFFFA0",
	groups = {dig_immediate = 3, not_in_creative_inventory = 1},
	sounds =	{ dig = { name = "", gain = 0 }, footstep = { name = "", gain = 0 },
			  dug = { name = "", gain = 0 }, player = { name = "", gain = 0 } },
	drop = "",
})

if(minetest.get_modpath("awards") ~= nil) then
	awards.register_achievement("magicbeans_w_beanstalk", {
		title = S("Sam and the Beanstalk"),
		description = S("Climb up a magic beanstalk and touch the clouds."),
		icon = "magicbeans_w_stem.png",
		trigger = {
			type = "dig",
			node = "magicbeans_w:cloud",
			target = 1,
		}
	})
end

local eat
if minetest.get_modpath("doc_items") then
	eat = doc.sub.items.temp.eat
end

local magicbeans_w_list = {
    { S("Magic jumping bean"), S("Magic super jump"), "jumping", nil, 5, nil, {"jump"},
        S("Eating this bean will make your jumps much, much higher for 30 seconds. In fact, you can jump so high that you may hurt yourself when you fall down again. Use with caution. It also cancels any previous jumping effects."),
        eat, },
    { S("Magic flying bean"), S("Magic flying"), "flying", 2, nil, 0.02, {"speed", "gravity"},
       S("Eating a flying bean will allow you to fly for 30 seconds, after which your flight will abruptly end. Use with caution. It also cancels out any gravity and walking speed effects you have."),
       S("Hold it in your hand, then leftclick to eat the bean. To initiate the flight, just jump. You will very slowly rise high in the skies and eventually sink again. Plan your flights carefully as falling down in mid-flight can be very harmful."), },
    { S("Magic running bean"), S("Magic super speed"), "running", 3, nil, nil, {"speed"},
       S("Eating a running bean will greatly increase your walking speed for 30 seconds. It also cancels out any previous walking speed effects."),
        eat, },
    { S("Magic beanstalk bean"), nil, "beanstalk", nil, nil, nil, nil,
       S("This magic bean will grow into a giant beanstalk when planted. The beanstalk can grow up to a height of about 100 blocks, easily reaching into the clouds."),
       S("Rightclick with it on dirt with grass (or any other block which allows saplings to grow) to plant it, then wait for a short while for the giant beanstalk to appear."),
    }
}

local pp_mod = minetest.get_modpath("playerphysics")

for i in ipairs(magicbeans_w_list) do
	
    local beandesc = magicbeans_w_list[i][1]
    local effdesc = magicbeans_w_list[i][2]
    local bean = magicbeans_w_list[i][3]
    local beanspeed = magicbeans_w_list[i][4]
    local beanjump = magicbeans_w_list[i][5]
    local beangrav = magicbeans_w_list[i][6]
    local beangroups = magicbeans_w_list[i][7]
    local longdesc = magicbeans_w_list[i][8]
    local usagehelp = magicbeans_w_list[i][9]
	
	--Register effect types
	if(beangroups ~= nil) then
		playereffects.register_effect_type("bean_"..bean, effdesc, "magicbeans_w_"..bean.."16.png", beangroups,
			function(player)
				if pp_mod then
					if beanspeed ~= nil then
						playerphysics.add_physics_factor(player, "speed", "magicbeans_w:speed", beanspeed)
					end
					if beanjump ~= nil then
						playerphysics.add_physics_factor(player, "jump", "magicbeans_w:jump", beanjump)
					end
					if beangrav ~= nil then
						playerphysics.add_physics_factor(player, "gravity", "magicbeans_w:gravity", beangrav)
					end
				else
					player:set_physics_override({speed=beanspeed, jump=beanjump, gravity=beangrav})
				end
			end,
			function(effect)
				local player = minetest.get_player_by_name(effect.playername)
				if pp_mod then
					playerphysics.remove_physics_factor(player, "speed", "magicbeans_w:speed")
					playerphysics.remove_physics_factor(player, "jump", "magicbeans_w:jump")
					playerphysics.remove_physics_factor(player, "gravity", "magicbeans_w:gravity")
				else
					local speed, jump, grav
					if beanspeed ~= nil then speed = 1 end
					if beanjump ~= nil then jump = 1 end
					if beangrav ~= nil then grav = 1 end
					player:set_physics_override({speed=speed, jump=jump, gravity=grav})
				end
			end
		)
	end

	local on_use = nil
	if bean ~= "beanstalk" then
		on_use = function(itemstack, user, pointed_thing)
			playereffects.apply_effect_type("bean_"..bean, 30, user)
			itemstack:take_item()
			return itemstack
		end
	end
	--Register beans
	minetest.register_craftitem("magicbeans_w:"..bean, {
		description = beandesc,
		_doc_items_longdesc = longdesc,
		_doc_items_usagehelp = usagehelp,
		inventory_image = "magicbeans_w_"..bean..".png",
		on_place = function(itemstack, placer, pointed_thing)
			if pointed_thing.type == "node" then
				if bean ~= "beanstalk" then
					minetest.add_item(pointed_thing.above, {name="magicbeans_w:"..bean})
				else
					-- Grow Beanstalk
					local stalk = pointed_thing.above
					local floor = pointed_thing.under
					if minetest.get_item_group(minetest.get_node(floor).name, "soil") == 0 then
						return itemstack
					end
					stalk.x = stalk.x - 2
					stalk.z = stalk.z - 2
					local height = 127 - stalk.y
					local c = {1, 1, 1, 1, 2, 1, 1, 1, 1}
					local d = {1, 2, 3, 6}
					local e = {9, 8, 7, 4}
					local ex = 0
					local zed = 1
					local blank = 0
					local why1, ex1, zed1, node

					local can_replace = function(pos)
						local node = minetest.get_node(pos).name
						if minetest.registered_nodes[node] then
							return minetest.registered_nodes[node].buildable_to
						end
						return false
					end

					for why = 0,height do
						blank = blank + 1
						if blank > 4 then blank = 1 end
						why1 = stalk.y + why
						for i = 1,9 do
							ex = ex + 1
							if ex > 3 then
								zed = zed + 1
								ex = 1
							end
							if c[i] == 1 then
								node = "magicbeans_w:leaves"
								if i == d[blank] or i == e[blank] then node = "magicbeans_w:blank" end
							else
								node = "magicbeans_w:stem"
							end
							ex1 = stalk.x + ex
							zed1 = stalk.z + zed
							local pos = {x=ex1, y=why1, z=zed1}
							if can_replace(pos) then
								minetest.set_node(pos,{name=node})
							elseif node == "magicbeans_w:stem" then
								-- Stop growing if stem hits an obstacle
								return itemstack
							end
						end
						zed = 0
					end
					-- Build cloud platform
					for ex = -10,20 do
						for zed = -10,20 do
							ex1 = stalk.x + ex
							zed1 = stalk.z + zed
							local pos = {x=ex1, y=why1, z=zed1}
							if can_replace(pos) then
								minetest.set_node(pos,{name="magicbeans_w:cloud"})
							end
						end	
					end				
				end
				itemstack:take_item()
			end
			return itemstack
		end,
		on_use = on_use,
	})	
	
end

-- Bean Spawning
minetest.register_abm(
	{nodenames = {"default:dirt_with_grass"},
	interval = 600,
	chance = 3000,
	action = function(pos)
		pos.y = pos.y + 1
	    math.randomseed(os.time())
		local j = math.random(4)
		local bean = magicbeans_w_list[j][3]
		minetest.add_item(pos, {name="magicbeans_w:"..bean})
	end,
})

-- Remove beanstalk blanks
minetest.register_abm(
	{nodenames = {"magicbeans_w:blank"},
	interval = 5,
	chance = 1,
	action = function(pos)
		local neighbors = {
			{ x=pos.x, y=pos.y, z=pos.z-1 },
			{ x=pos.x, y=pos.y, z=pos.z+1 },
			{ x=pos.x, y=pos.y-1, z=pos.z },
			{ x=pos.x, y=pos.y+1, z=pos.z },
			{ x=pos.x-1, y=pos.y, z=pos.z },
			{ x=pos.x+1, y=pos.y, z=pos.z },
		}
		local attached = false
		for i=1,#neighbors do
			local node = minetest.get_node_or_nil(neighbors[i])
			if(node ~= nil) then
				if node.name == "magicbeans_w:leaves" or node.name == "magicbeans_w:stem" then
					attached = true
					break
				end
			end
		end
		if(attached == false) then
			minetest.remove_node(pos)
		end
	end
})


